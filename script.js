class Contact {
	constructor(firstnameInstance, lastnameInstance, emailInstance) {
		this.firstname = this.formatUserName(firstnameInstance);
		this.lastname = this.formatUserName(lastnameInstance, true);
		this.email = emailInstance;
		this.creationDate = new Date();
	}

	/**
	 * Recupère le nom complet de l'instance
	 * 
	 * @returns {String} - Retourne le nom complet de l'instance
	 */
	getFullName() {
		return this.firstname + ' ' + this.lastname;
	}

	/**
	 * Création de la date au format français
	 * 
	 * @returns {String} - Retourne la date au format français
	 */
	getFrenchFormatDate() {
		// const options = { weekday: 'long', year: 'numeric', month: 'numeric', day: 'numeric' };
		const frenchDays = ['Dimanche', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi'];

		let frenchDate = frenchDays[this.creationDate.getDay()] + ' ' + this.creationDate.toLocaleDateString('fr-FR');
		return frenchDate;
	}

	/**
	 * Créer une ligne à ajouter dans le tableau HTML
	 * 
	 * @returns {HTMLTableRowElement} - Retourne une ligne de tableau HTML avec les données de l'instance
	 */
	addTableRow() {
		let values = [this.firstname, this.lastname, this.email, this.getFrenchFormatDate()]; 
		let newTableRow = document.createElement('tr');
		for (let i = 0; i < values.length; i++) {
			let newTableCell = document.createElement('td');
			newTableCell.textContent = values[i];
			newTableRow.appendChild(newTableCell);
		}
		return newTableRow;
	}

	/**
	 * Formate le nom d'utilisateur pour gérer la casse les noms composés 
	 * 
	 * @param {String} nameToFormat - Le nom à formater
	 * @returns {String} Le nom formaté : Majuscule en début de Nom et tiret si nom composé
	 */
	formatUserName(nameToFormat, isLastname = false) {
		// Je trime pour enlever les espace avant et après et je mets en minuscule
		let userName = nameToFormat.toLowerCase().trim();
		let userNames = [];
		let isLastnameSpaceSeparator;
	
		/**
		 * Capitalize a Name
		 * 
		 * @param {String} value - Name to Capitalize
		 * @returns {String} - Name capitalized
		 */
		const capitalize = function(value) {
			return value.trim().charAt(0).toUpperCase() + value.slice(1)
		} 
	
	
		// Je teste si c'est un prénom/nom composé (avec un espace ou un tiret) et le range les différents nom dans le tableau userNames;
		if (/[-]/.test(userName)) {
			isLastnameSpaceSeparator = false;
			userNames = userName.split("-");
		} else if (/[\s]+/.test(userName)) {
			isLastnameSpaceSeparator = true;
			userNames = userName.split(" ");
		}
	
		// Si c'est un nom composé, le tableau contient plusieurs éléments
		if (userNames.length > 0) {
			//Je crée un nouveau tableau qui contiendra les noms capitalisés
			let tempNames = [];
			// Je boucle sur les noms du tableau userNames
			for (let i=0; i<userNames.length; i++) {
				// Je push le nom en majuscule dans le tableau tempNames
				tempNames.push(capitalize(userNames[i]));
			}
			// Si c'est un nom de famille composé et les noms sont séparés par un espace, je crée un nom compsé séparé par un espace, dans les autres cas je crée un nom composé séparé par un tiret 
			(isLastname && isLastnameSpaceSeparator)  ? userName = tempNames.join(" ") : userName = tempNames.join("-");
		} else {
			// Si ce nom n'est pas composé, je le capitalise
			userName = capitalize(userName);
		}
	
		// Et je le renvoie.
		return userName;
	}

}

// Je crée 2 instance que je range dans un tableau
const cLevisse = new Contact('Carole', 'Levisse', 'clevisse@gmail.com');
const mNelsonne = new Contact('Mélodie', 'Nelsonne', 'mnelsonne@gmail.com');
const contacts = [cLevisse, mNelsonne];


// Mes éléments du DOM utiles en global
let contactForm = document.querySelector('form');
let toggleContactButton = document.querySelector('#toggle-contact');

/**
 * Création du tableau des contacts 
 * en se basant sur un array contenant les titres
 * Les TR contenant les contact sont gérés pas la methéde addTableRow de la classe Contact
 */
function createContactTable() {
	const tableHeadContent = ['Prénom', 'Nom', 'Email', 'Date de création', ''];

	// Création du tableau avec les classes bootstrap
	let newTable = document.createElement('table');
	newTable.classList.add('table', 'table-striped', 'mt-3');

	// Creéation de la partie en-tête
	let newTableHead = document.createElement('thead');
	let newTableRow = document.createElement('tr');

	tableHeadContent.forEach(function(title) {
		let newTableTitle = document.createElement('th');
		newTableTitle.textContent = title;
		newTableRow.appendChild(newTableTitle);
	})
	newTableHead.appendChild(newTableRow);

	// Création du corps du tableau
	let newTableBody = document.createElement('tbody');
	// Création  d'autant de lignes que d'instance dans le tableau contacts
	// contacts.forEach(function(contact) {
	// 	let newContactTableRow = contact.addTableRow();
	// 	newTableBody.appendChild(newContactTableRow);
	// })

	for (let i=0; i < contacts.length; i++) {
		let newContactTableRow = contacts[i].addTableRow();
		newTableBody.appendChild(newContactTableRow);
	}

	//Ajout des boutons de suppression
	addDeleteButton(newTableBody.children);

	// Insertion des thaed et tbody dans la table 
	newTable.append(newTableHead, newTableBody);


	// puis insertion de la table dans le DOM en tant qu'enfant du container
	document.querySelector('.container').appendChild(newTable);
} 


function addDeleteButton(tableRows) {
	for (let i=0; i<tableRows.length; i++) {
		let newTableCell = document.createElement('td');
		let newDeleteButton = document.createElement('i');
		newDeleteButton.id = 'delete-button';
		newDeleteButton.classList.add('fa-solid', 'fa-trash', 'text-danger');
		newDeleteButton.style.cursor = "pointer";
		newDeleteButton.addEventListener('click', function() {
			contacts.splice(i, 1);
			launchBootstrapToast({
				title: "C'est fait !",
				content: "Le contact a bien été supprimé !"
			})
			toggleContactTable();
		})
		newTableCell.appendChild(newDeleteButton);
		tableRows[i].appendChild(newTableCell);
	}
}

/**
 * Test la validité du mail avec un regex simplifié
 * 
 * @param {String} mail - Mail entré par l'utilisateur
 * @returns {Boolean} 
 */
  function checkMailValidity(mail) {
    let regex = /^[a-z][a-z0-9\-\.]+[a-z0-9]@[a-z0-9\-\.]+\.[a-z.]{2,5}$/i;
    if (regex.test(mail.toLowerCase().trim())) {
        return true;
    } else {
		launchBootstrapToast({
			title: "Attention",
			content: "Le mail n'est pas valide !"
		}, true)
        return false;
    }
}

/**
 * Ajoute une instance de contact dans le tableau contacts
 * en utilisant les valeur des champs imput remplis par l'utilisateur
 */
function addContact() {
	let message = {
		title: "",
		content: ""
	};
	let firstnameUserValue = document.querySelector('#firstname').value;
	let lastnameUserValue = document.querySelector('#lastname').value;
	let emailUserValue = document.querySelector('#email').value;


	if (firstnameUserValue && lastnameUserValue && emailUserValue) {
		// Je vérifie que le mail est bon
		let isEmailValidated = checkMailValidity(emailUserValue);
		// Si c'est pas bon, j'arrète la fonction
		if (!isEmailValidated) return false;
		// Je crée une instance de contact
		let newContact = new Contact(firstnameUserValue, lastnameUserValue, emailUserValue);
		// Je l'ajoute dans le tableau
		contacts.push(newContact);
		message.title = "Bravo !";
		message.content = "Le contact a bien été ajouté !";
		launchBootstrapToast(message);
		contactForm.reset();
		// Je recharge la table contact si elle était visible
		toggleContactTable();
	} else {
		message.title = "Attention";
		message.content = "Au moins un des champs est vide !";
		launchBootstrapToast(message, true);
		return false;
	}

		
}

function toggleContactTable(event) {
	if (toggleContactButton.textContent === "Voir les contacts") {
		createContactTable();
		toggleContactButton.textContent = "Masquer les contacts";
	} else if (!event || event.target.id === "delete-button") {
		document.querySelector('table').remove();
		createContactTable();
	} else {
		document.querySelector('table').remove();
		toggleContactButton.textContent = "Voir les contacts";
	}
}

/**
 * Lance le toast de Bootstrap pour informer l'utilisateur
 * 
 * @param {Object} msg - Le titre et le content du toast
 */
function launchBootstrapToast(msg, isError = false) {
	let toastBootstrap = document.getElementById('liveToast');
	let toastHeader = toastBootstrap.querySelector('.toast-header');
	let toastTitle = toastHeader.querySelector('strong');
	let toastIcon = toastHeader.querySelector('i');
	let toastContent = toastBootstrap.querySelector('.toast-body');
	if (isError) {
		toastIcon.classList.remove('fa-circle-check', 'text-success');
		toastIcon.classList.add('fa-circle-exclamation','text-danger');
	} else {
		toastIcon.classList.remove('fa-circle-exclamation','text-danger');
		toastIcon.classList.add('fa-circle-check','text-success');
	}
	toastTitle.textContent = msg.title;
	toastContent.textContent = msg.content;
	let toast = new bootstrap.Toast(toastBootstrap)
	toast.show()
}

// A la soumission du formulaire
contactForm.addEventListener('submit', function(e) {
	e.preventDefault();
	addContact();
})

// Au clic sur le bouton toggleContact
toggleContactButton.addEventListener('click', toggleContactTable)